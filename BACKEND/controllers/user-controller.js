import Job from  "../model/Job.js";
import { v4 as uuidv4 } from "uuid";
export const getalljobs=async(req,res,next)=>{
    let job;
    try{
        job=await Job.find();

    }catch(err){
        console.log(err);
    }
    if(!job){
        return res.status(404).json({message:"No users found"});
    }
    return res.status(200).json({job})
};


export const createJob = async (req, res, next) => {
    const { title, description,minexperience,maxexperience, status, createdBy, createdAt } = req.body;
  
    // Generate a unique ID for the new job
    const id = uuidv4();
  
    const existingJob = await Job.findOne({ id });
  
    if (existingJob) {
      return res.status(400).json({ message: "Job already exists" });
    }
  
    const job = new Job({
      id,
      title,
      description,
      minexperience,
      maxexperience,
      status,
      createdBy,
      createdAt,
    });
  
    try {
      await job.save();
    } catch (err) {
      return console.log(err);
    }
  
    return res.status(201).json({ job });
  };
export const updateJob=async(req,res,next)=>{
    const {status}=req.body;
    const userid = req.params.id;

    let nuser;
    try{
    nuser=await  Job.findOneAndUpdate({id:userid},{
        status,
        modifiedBy:"UpdatedUser",
        modifiedAt: new Date(),
       

    },
    {new:true}
    );
    
}catch(err){
    return console.log(err);

}
if(!nuser){
    return res.status(500).json({message:"unable to update"})

}
return res.status(200).json({nuser})
} ;

export const getbyid=async(req,res,next)=>{
    
    const userid = req.params.id;

    let nuser;
    try{
    nuser=await  Job.findOne({id:userid});
    
}catch(err){
    return console.log(err);

}
if(!nuser){
    return res.status(500).json({message:"No Details found"})

}
return res.status(200).json({nuser})
} ;

export const deleteuser = async (req, res, next) => {
    const userId = req.params.id; // Assuming userId is a UUID string
  
    try {
      const deletedUser = await Job.findOneAndDelete({ id: userId });
  
      if (!deletedUser) {
        return res.status(404).json({ message: "User not found" });
      }
  
      return res.status(200).json({ message: "Successfully deleted" });
    } catch (err) {
      console.error(err);
      return res.status(500).json({ message: "Unable to delete" });
    }
  };
  
