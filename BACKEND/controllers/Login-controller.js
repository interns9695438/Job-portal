import Login from "../model/Login.js";
import bcrypt from "bcryptjs";


export const getAll=async(req,res,next)=>{//hhtp req are alway in ansyn task
    let users;
    try{
        users=await Login.find();

    }catch(err){
        console.log(err);

    }
    if(!users){
        return res.status(404).json({message:"No users Found"});// not found
    }
    return res.status(200).json({users});//ok successs
};

export const signup =async(req,res,next)=>{
    const {username,password}=req.body;
    let existingUser;
    try{
        existingUser= await  Login.findOne({username});


    }catch(err){
        console.log(err);
    }
    if(existingUser){
        return res.status(400).json({message:"User Already exists"})
    }
    const hashedPassword = bcrypt.hashSync(password);
    const user =new Login({
        username,
        password:hashedPassword,

    });
    
    try{
       await  user.save();//to save the data insed the database

    }catch(err){
        console.log(err);
    }
    return res.status(201).json({user});
};
export const login = async(req,res,next)=>{
    const{username,password}=req.body;
    let existingUser;

    try{
        existingUser = await Login.findOne({username});

    }catch(err){
        console.log(err);
    }
    if(!existingUser){
        return res.status(404).json({message:"couldn't fimd the user  by this username"});
    }
    const isPasswordCorrect=bcrypt.compareSync(password,existingUser.password);
    if(!isPasswordCorrect){
        return res.status(400).json({message:"Incorrect password"});
    }
    return res.status(200).json({message:"Login Successfull!"})

};

