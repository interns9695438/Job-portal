import express from "express";
import { createJob, deleteuser, getalljobs, getbyid, updateJob} from "../controllers/user-controller.js";

const router=express.Router();
router.get("/jobs/get-all-jobs",getalljobs);
router.post("/jobs",createJob); 
router.put('/jobs/:id',updateJob);
router.get('/job/:id',getbyid);
router.delete("/:id",deleteuser);

export default router;