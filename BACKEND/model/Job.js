import mongoose from "mongoose";
import { v4 as uuidv4 } from "uuid";

const Schema=mongoose.Schema;

const jobSchema=new Schema({
  id: {
    type: String, // Use String type for UUID
    default: uuidv4,
  },
    

    title:{
        type:String,
        required:true
    },
    description:{
        type:String,
        required:true
    },
    minexperience:{
      type:String,
      required:true

    },
    maxexperience:{
      type:String,
      required:true

    },
  
    status:{
        type:String,
        enum:["Active","Closed"],
        required:true
    },
   
    createdBy:{
        type:String,
        default:"Admin"
    },
    createdAt:{
        type:Date,
        default: Date.now
        
    },
    modifiedBy:{
        type:String
    },
    modifiedAt:{
        type:Date
    }

    

});
jobSchema.pre("updateOne",{ document: true }, function (next) {

    const updateFields = this.getUpdate();
  
    if (updateFields.status) {
  
      updateFields.modifiedBy = "Admin"; // Replace with the admin's username or ID
  
      updateFields.modifiedAt = new Date();
  
    }
  
    next();
  
  });
  jobSchema.pre('save', function (next) {

    if (this.isNew) {
  
      this.createdate = new Date();
  
      this.createdby = 'Admin'; // Replace with the desired admin username Id
  
    }
  
    next();
  
  });
 
  


export default mongoose.model("Job",jobSchema);