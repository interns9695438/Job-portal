import React, { useEffect, useState } from "react";
import axios from "axios";

function formatExperience(minexperience, maxexperience) {
  return `${minexperience}-${maxexperience} years`;
}

function JobList() {
  const [jobs, setJobs] = useState([]);

  useEffect(() => {
    const fetchJobs = async () => {
      // Fetch the list of jobs from the backend
      fetch("http://localhost:5002/api/jobs/get-all-jobs")
        .then((response) => {
          if (!response.ok) {
            throw new Error("Network response was not ok");
          }
          return response.json();
        })
        .then((data) => {
          setJobs(data.job); // Update the state with the job data
        })
        .catch((error) => {
          console.error("Error:", error);
        });
    };
    fetchJobs();
    const pollInterval = setInterval(fetchJobs, 5001);
    return () => clearInterval(pollInterval);
  }, []);

  const handleChangeStatus = (jobId, newStatus) => {
    axios
      .put(`http://localhost:5002/api/jobs/${jobId}`, { status: newStatus })
      .then((response) => {
        if (response.status === 200) {
          const updatedJobs = jobs.map((job) =>
            job.id === jobId ? { ...job, status: newStatus } : job
          );
          setJobs(updatedJobs);
        } else {
          console.error("Error updating job:", response.statusText);
        }
      })
      .catch((error) => {
        console.error("Error updating job:", error);
      });
  };

  return (
    <div className="container mt-4">
      <h1 className="mb-4" style={{ fontSize: "24px" }}>
        Job Search
      </h1>
      <div className="row">
        <ul className="list-group">
          {jobs.map((job) => (
            <li key={job.id} className="list-group-item">
              <div className="d-flex justify-content-between align-items-center">
                <div>
                  <h2 className="text-primary" style={{ fontSize: "22px" }}>
                    {job.title}
                  </h2>
                </div>
                <div className="d-flex align-items-center">
                  {/* Wrap experience and status in a flex container */}
                  <div>
                    <p className="text-end">
                      Experience: {formatExperience(job.minexperience, job.maxexperience)}
                    </p>
                  </div>
                  <div className="mx-3">
                    {/* Add margin for spacing */}
                    <p>
                      Status:{" "}
                      <span className="fw-bold text-primary">{job.status}</span>
                    </p>
                  </div>
                </div>
              </div>
              <div className="border p-5">
                <p>{job.description}</p>
              </div>

              <div className="d-flex align-items-center mt-5">
                <div>
                  <p>Created By: {job.createdBy}</p>
                </div>
                <div className="mx-4">
                  <p>Created At: {new Date(job.createdAt).toLocaleString()}</p>
                </div>
                <div className="mx-4">
                  {job.modifiedBy && (
                    <p>Closed By: {job.modifiedBy}</p>
                  )}
                </div>
                <div className="mx-4">
                  {job.modifiedAt && (
                    <p>closed At: {new Date(job.modifiedAt).toLocaleString()}</p>
                  )}
                </div>
              </div>

              {job.status === "Active" && (
                <div className="text-end">
                  <button
                    className="btn btn-primary"
                    onClick={() => handleChangeStatus(job.id, "Closed")}
                  >
                    Close
                  </button>
                </div>
              )}
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
}

export default JobList;
