import React, { useState } from 'react';
import { useNavigate, Link } from 'react-router-dom';
import axios from 'axios';

const LoginPage = () => {
  const isValidPassword = (value) =>
    /^(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,}$/.test(value);

  const isValidEmail = (value) =>
    /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/.test(value);

  let navigate = useNavigate();

  const handleClick = () => {
    console.log('Navigating to /jobfrom');
    navigate('/jobfrom');
  };

  const [login, setLogin] = useState({
    username: '',
    password: '',
  });

  const [errors, setErrors] = useState({});

  const handleChange = (e) => {
    const { name, value } = e.target;
    setLogin({
      ...login,
      [name]: value,
    });
    setErrors((prevErrors) => ({
      ...prevErrors,
      [name]: '',
    }));
  
  };
 
  const validateForm = () => {// validate 
    let valid = true;

    if (!isValidEmail(login.username)) {
      setErrors((prevErrors) => ({
        ...prevErrors,
        username: 'Invalid email address.',
      }));
      valid = false;
    }

    if (!isValidPassword(login.password)) {
      setErrors((prevErrors) => ({
        ...prevErrors,
        password:
          'Password must start with a capital letter, contain at least one digit, one special character, and have a minimum length of 6 characters.',
      }));
      valid = false;
    }

    return valid;
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    // Define the validateForm function to perform form validation
    console.log('Before form validation:', login);
    const isValid = validateForm();
    console.log('After form validation:', isValid);

    if (isValid) {
      console.log('Sending login data to the backend:', login);

      axios
        .post('http://localhost:5002/log/enter', login)
        .then((response) => {
          console.log('Response from the backend:', response.data);

          console.log(response.data.message); // Log the message received from the backend

          if (response.data.message === 'Login Successfull!') {
            console.log('Authentication succeeded');
            handleClick();
          } else {
            console.log('Authentication failed');
            console.log('Authentication failed message:', response.data.message);
          }
          setLogin({
            username: '',
            password: '',
          });
          setErrors({});
        })
        .catch((err) => {
          console.log('Error from the backend:', err);
          console.log('error:', err);
        });
    }
  };

  return (
    <>
      <nav className="navbar navbar-expand-lg navbar-light bg-light " style={{ position: 'sticky', top: '0', zIndex: '100' }}>
        <div className="container-fluid">
          <Link className="navbar-brand fw-bold " href="#">
            Job Portal
          </Link>
        </div>
      </nav>

      <div className="container my-4 ">
        <div className="col-md-5">
          <form onSubmit={handleSubmit}>
            <div className="mb-3">
              <label htmlFor="username" className="form-label">
                Email
              </label>
              <input
                type="text"
                className={`form-control ${errors.username && 'is-invalid'}`}
                id="username"
                name="username"
                value={login.username}
                onChange={handleChange}
              />
              {errors.username && (
                <div className="invalid-feedback">{errors.username}</div>
              )}
            </div>
            <div className="mb-3">
              <label htmlFor="password" className="form-label">
                Password
              </label>
              <input
                type="password"
                className={`form-control ${errors.password && 'is-invalid'}`}
                id="password"
                name="password"
                value={login.password}
                onChange={handleChange}
              />
              {errors.password && (
                <div className="invalid-feedback">{errors.password}</div>
              )}
            </div>

            <button type="submit" className="btn btn-primary">
              Login
            </button>
          </form>
        </div>
      </div>
    </>
  );
};

export default LoginPage;
