import React, { useState } from "react";
import axios from "axios";
import "bootstrap/dist/css/bootstrap.min.css";
import { Modal } from "react-bootstrap";
import JobList from "./JobList";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Link } from 'react-router-dom';

function JobForm() {
  const [loggedIn, setLoggedIn] = useState(false);

  const isNotEmpty = (value) => value.trim() !== "";
  const isValidDescription = (value) => value.length >= 10;
  const isValidExperience = (value) => isNotEmpty(value);

  const [job, setJob] = useState({
    title: "",
    description: "",
    status: "Active",
    minexperience: "",
    maxexperience: "",
  });

  const [errors, setErrors] = useState({});
  const [showConfirmation, setShowConfirmation] = useState(false);

  const validateForm = () => {
    let valid = true;

    if (!isNotEmpty(job.title)) {
      setErrors((prevErrors) => ({
        ...prevErrors,
        title: "Title is required.",
      }));
      valid = false;
    }

    if (!isValidDescription(job.description)) {
      setErrors((prevErrors) => ({
        ...prevErrors,
        description: "Description should be at least 10 characters.",
      }));
      valid = false;
    }

    if (!isValidExperience(job.minexperience)) {
      setErrors((prevErrors) => ({
        ...prevErrors,
        minexperience: "Minimum experience is required",
      }));
      valid = false;
    }

    if (!isValidExperience(job.maxexperience)) {
      setErrors((prevErrors) => ({
        ...prevErrors,
        maxexperience: "Maximum experience is required",
      }));
      valid = false;
    }

    if (job.minexperience !== "" && job.maxexperience !== "") {
      if (parseInt(job.minexperience) > parseInt(job.maxexperience)) {
        setErrors((prevErrors) => ({
          ...prevErrors,
          minexperience: "Minimum experience cannot be greater than Maximum experience",
          maxexperience: "Maximum experience cannot be less than Minimum experience",
        }));
        valid = false;
      }
    }

    return valid;
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setJob({
      ...job,
      [name]: value,
    });

    setErrors((prevErrors) => ({
      ...prevErrors,
      [name]: "",
    }));
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    if (validateForm()) {
      axios
        .post("http://localhost:5002/api/jobs", job)
        .then((response) => {
          console.log(response.data);
          setJob({
            title: "",
            description: "",
            status: "Active",
            minexperience: "",
            maxexperience: "",
          });
          setErrors({});
          setShowConfirmation(false);
          toast.success("Job created successfully!", {
            position: "top-right",
            autoClose: 3000, 
          });
        })
        .catch((error) => {
          console.error("Error:", error);
        });
    }
  };

  const handleReset = () => {
    setJob({
      title: "",
      description: "",
      status: "Active",
      minexperience: "",
      maxexperience: "",
    });

    setErrors({});
  };

  const showConfirmationModal = () => {
    setShowConfirmation(true);
  };

  const closeConfirmationModal = () => {
    setShowConfirmation(false);
  };

  const brandStyle = {
    fontSize: "28px",
  };
  
  const brandStyle1 = {
    fontSize: "15px",
  };

  const handleLogout = () => {
    setLoggedIn(false);
  };

  return (
    <div>
      <nav className="navbar navbar-expand-lg navbar-light bg-primary " style={{ position: 'sticky', top: '0', zIndex: '100' }}>
        <div className="container-fluid">
          <Link className="navbar-brand fw-bold text-light" to="#" style={brandStyle}>
            Job Portal
          </Link>
          <div className="ml-auto d-flex align-items-center">
            {loggedIn ? (
              <button className="btn btn-link" onClick={handleLogout}>
                Logout
              </button>
            ) : (
              <button className="btn btn-link" onClick={() => setLoggedIn(true)}>
                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="black" class="bi bi-person-circle" viewBox="0 0 16 16">
                  <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z" />
                  <path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z" />
                </svg>
              </button>
            )}
          </div>
        </div>
      </nav>

      <nav className="navbar navbar-expand-lg navbar-light bg-light  border border-blue " style={{ position: 'sticky', top: '56px', zIndex: '100' }}>
        <Link className="navbar-brand  container" href="#" style={brandStyle1} onClick={showConfirmationModal}>
          Create Job
        </Link>
      </nav>

      <Modal show={showConfirmation} onHide={closeConfirmationModal}>
        <Modal.Header closeButton>
          <Modal.Title>Create Job</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form onSubmit={handleSubmit}>
            <div className="mb-3">
              <label htmlFor="title" className="form-label">
                Title<span className="text-danger">*</span>
              </label>
              <input
                type="text"
                className={`form-control ${errors.title && "is-invalid"}`}
                name="title"
                value={job.title}
                onChange={handleChange}
                placeholder="Enter job title"
              />
              {errors.title && (
                <div className="invalid-feedback">{errors.title}</div>
              )}
            </div>
            <div className="mb-3">
              <label htmlFor="description" className="form-label">
                Description<span className="text-danger">*</span>
              </label>
              <textarea
                name="description"
                className={`form-control ${errors.description && "is-invalid"}`}
                value={job.description}
                onChange={handleChange}
                placeholder="Enter job description"
              ></textarea>
              {errors.description && (
                <div className="invalid-feedback">{errors.description}</div>
              )}
            </div>
            <div className="mb-3 row">
              <label className="col-sm-3 col-form-label">Experience:</label>
              <div className="col-sm-4">
                <select
                  name="minexperience"
                  className={`form-select ${errors.minexperience && "is-invalid"}`}
                  value={job.minexperience}
                  onChange={handleChange}
                >
                  <option value=""> Min</option>
                  {Array.from({ length: 10 }, (_, index) => (
                    <option key={index} value={index}>
                      {index}
                    </option>
                  ))}
                </select>
                {errors.minexperience && (
                  <div className="invalid-feedback">
                    {errors.minexperience}
                  </div>
                )}
              </div>
              <div className="col-sm-4">
                <select
                  name="maxexperience"
                  className={`form-select ${errors.maxexperience && "is-invalid"}`}
                  value={job.maxexperience}
                  onChange={handleChange}
                >
                  <option value="">max</option>
                  {Array.from({ length: 20 }, (_, index) => (
                    <option key={index} value={index}>
                      {index}
                    </option>
                  ))}
                </select>
                {errors.maxexperience && (
                  <div className="invalid-feedback">
                    {errors.maxexperience}
                  </div>
                )}
              </div>
            </div>
            <div className="d-flex justify-content-between align-items-center">
             <button type="Reset" className="btn btn-secondary" onClick={closeConfirmationModal}>Close</button>
              <div className="d-grid gap-2 d-md-flex justify-content-md-end">
               <button type="submit" className="btn btn-primary me-md-2">
                Create
            </button>
             <button
               type="reset"
               className="btn btn-secondary"
               onClick={handleReset}
            >
            Clear
          </button>
        </div>
      </div>
          </form>
        </Modal.Body>
      </Modal>
      <ToastContainer position="top-right" autoClose={3000} />
      <div>
        <JobList />
      </div>
    </div>
  );
}

export default JobForm;
