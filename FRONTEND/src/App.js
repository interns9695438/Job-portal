import React from "react";
import LoginPage from "./components/LoginPage";
import {BrowserRouter as Main, Route, Routes} from 'react-router-dom';
import JobForm from "./components/JobFrom";



function App() {
 
  return (
    <>
    <Main>
      
        <Routes>
        <Route exact path="/" element={<LoginPage/>}/>
          <Route exact path="/jobfrom" element={<JobForm/>}/>
        </Routes>
    </Main>
    </>
  );
}

export default App;
